from django.conf.urls.defaults import *
from django.conf import settings

urlpatterns = patterns('portal.views',
    url(r'^$', 'main', name='main'),
    url(r'^flush/$', 'flush', name='flush'),
    url(r'^signin/$', 'signin', name='signin'),
    url(r'^signout/$', 'signout', name='signout'),
    url(r'^info_detail/$', 'info_detail', name='info_detail'),
    url(r'^delete_deploy/$', 'delete_deploy', name='delete_deploy'),
    url(r'^command/$', 'command', name='command'),    
  
    url(r'^deploying/$', 'deploying', name='deploying'),
)
