# -*- coding: utf-8 -*-
from django.core.cache import cache
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.views.generic.simple import direct_to_template
from django.shortcuts import get_object_or_404, render_to_response
from django.template import Context, Template, RequestContext
from django.core import serializers
from django.core import urlresolvers
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse
from django.contrib import auth
from django.conf import settings
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.utils.encoding import smart_str, smart_unicode
from django.contrib.auth.decorators import login_required
from django import forms

from pymongo import DESCENDING, ASCENDING
import simplejson
import pexpect
import os
import uuid
import pymongo
import urllib2
import ssh_lord
import commands
import re

from datetime import datetime, timedelta

# set database config (db name=upt, collection=deploy,log & checker)
db = pymongo.Connection()
coll_deploy = db.upt.deploy
coll_log = db.upt.log
coll_checker = db.upt.checker

def main(request):
    if request.GET.get('not_found'):
        return HttpResponseRedirect('/')
    server_list = coll_deploy.find()
    return render_to_response('main.html', locals(), context_instance=RequestContext(request))

@login_required(redirect_field_name='not_found')
def portal(request):
    return HttpResponse()

@login_required(redirect_field_name='not_found')
def command(request):
    try:
        ip = request.POST.get('ip')
        urllib2.urlopen('http://%s:1212/reboot' % ip)
    except Exception, err:
        print err
    return HttpResponse(True)

@login_required(redirect_field_name='not_found')
def delete_deploy(request):
    if request.POST.get('key'):
        coll_deploy.remove({'key': request.POST.get('key')})
        coll_log.remove({'key': request.POST.get('key')})
        return HttpResponse(True)
    return HttpResponse(False)

def flush(request):
    coll_deploy.remove()
    coll_log.remove()
    coll_checker.remove()
    return HttpResponseRedirect(reverse('main'))

@login_required(redirect_field_name='not_found')
def info_detail(request):
    server = coll_log.find_one({'key': request.GET.get('server')})
    server_info = []
    timestamp = datetime.now()
    realtime = True
    try:
        timestamp = datetime(int(request.GET.get('date').split('-')[0]), int(request.GET.get('date').split('-')[1]), int(request.GET.get('date').split('-')[2]))
    except Exception, err:
        print err
    else:
        print timestamp
        if timestamp != datetime(datetime.now().year, datetime.now().month, datetime.now().day):
            realtime = False
            print 'not realtime'

    for i in coll_log.find({'key': request.GET.get('server')}).sort('_id', direction=ASCENDING):
        if i['timestamp'].day == timestamp.day and i['timestamp'].month == timestamp.month and i['timestamp'].year == timestamp.year:
            try:
                listen_data = []
                for x in i['listen'].keys():
                    listen_data.append({'port':x, 'name':i['listen'][x][0]})
                i.update({'listen':listen_data})
            except Exception:
                pass
            server_info.append(i)
    
    server_info_log = []
    for i in coll_log.find({'key': request.GET.get('server')}).sort('_id', direction=DESCENDING):
        if i['timestamp'].day == timestamp.day and i['timestamp'].month == timestamp.month and i['timestamp'].year == timestamp.year:
            try:
                listen_data = []
                for x in i['listen'].keys():
                    listen_data.append({'port':x, 'name':i['listen'][x][0]})
                i.update({'listen':listen_data})
            except Exception:
                pass
            server_info_log.append(i)


    paginator = Paginator(server_info_log, 10)
    try:
        try:
            page = int(request.GET.get('page', '1'))
        except ValueError:
            page = 1

        try:
            current_page = paginator.page(page)
        except (EmptyPage, InvalidPage):
            current_page = paginator.page(paginator.num_pages)

        server_info_log = current_page.object_list
        pages = paginator.page_range
    except Exception, err:
        pass
        
    if request.POST.get('ajax') == 'true':
        return render_to_response('ajax_table.html', locals(), context_instance=RequestContext(request))
    return render_to_response('info_detail.html', locals(), context_instance=RequestContext(request))

@login_required(redirect_field_name='not_found')
def deploying(request):
    try:
        sudo_password = request.POST.get('sudo')
        CUR_PATH = os.path.abspath(os.path.dirname(__file__))
        deploy_data = {'key': str(uuid.uuid1()),
                   'username':request.POST.get('ssh_username'),
                   'password':request.POST.get('ssh_password'),
                   'port':request.POST.get('ssh_port'),
                   'ip':request.POST.get('ip'),
                   'label': request.POST.get('label'),
                   'up': False,
                   'email': request.POST.get('email'),
                   'hp': request.POST.get('hp'),
                   'timestamp': '%s-%s-%s-%s-%s-%s-%s' % (datetime.now().year,
                                                          datetime.now().month,
                                                          datetime.now().day, 
                                                          datetime.now().hour, 
                                                          datetime.now().minute,
                                                          datetime.now().second,
                                                          datetime.now().microsecond
                                                         )
                   }
        if not request.POST.get('label') or request.POST.get('label') == '':
            return HttpResponse('Nama Label Server Harus Diisi')
        deploy_data = simplejson.dumps(deploy_data)    
        open(os.path.join(CUR_PATH, 'deploying.json'),'w').write(deploy_data)
        try:
            ssh_lord.Connection(request.POST.get('ip'), username=request.POST.get('ssh_username'), password=request.POST.get('ssh_password'), port=int(request.POST.get('ssh_port')))
        except Exception, err:
            return HttpResponse('%s: Koneksi SSH Gagal' % err)
        
        # eksekusi fab untuk deploying
        deployer = commands.getoutput('fab remote copy')
        if re.search('No existing session', deployer): # jika gagal maka perlu menggunakan sudo password
            deployer = commands.getoutput('echo "%s" | sudo -S fab remote copy' % sudo_password) # eksekusi ulang fab menggunakan sudo password
            if re.search('incorrect password attempts', deployer): # jika gagal kembali maka password sudo salah
                return HttpResponse('Sudo Password Salah') # response password salah
        if re.search('Done.', deployer): # jika deploy berhasil
            try:
                if coll_deploy.find_one({'ip':request.POST.get('ip')}):
                    return HttpResponse('IP sudah pernah di deploy') # jika duplikasi deploy ditemukan
                coll_deploy.insert(simplejson.loads(deploy_data))
            except Exception, err:
                print err
            print deployer
            return HttpResponse('True') # response deploy berhasil
    except Exception, err:
        return HttpResponse(err)

            
#        session = pexpect.spawn('fab remote copy')
#    except Exception, err:
#        return HttpResponse(err)
    
#    try:
#        session.expect('.*No existing session.*')
#    except Exception, err:
#        return HttpResponse(err)
#    else:
#        session = pexpect.spawn('sudo fab remote copy')
    
#    try:
#        session.expect('\[sudo\] password for .*:')
#    except Exception, err:
#        pass
#    else:
#        print 'sending %s' %sudo_password
#        session.sendline(sudo_password)

#    try:
#        session.expect('.*Sorry, try again.')
#    except Exception, err:
#        pass
#    else:
#        return HttpResponse('Gagal')

#    try:
#        session.expect('.* password for .*:', timeout=3)
#    except pexpect.ExceptionPexpect:
#        pass
#    else:
#        print 'sending %s' %request.POST.get('ssh_password')
#        session.sendline(request.POST.get('ssh_password'))
#    try:
#        session.interact()
#    except:
#        pass

def signin(request):
    if request.method == 'POST':
        try:
            getuser = auth.authenticate(username=request.POST.get('username'), password=request.POST.get('password'))
            auth.login(request, getuser)
        except Exception, err:
            result = err
        else:
            result = 'True'
        return HttpResponse(result)
    return HttpResponseRedirect(reverse('main'))

@login_required(redirect_field_name='not_found')
def signout(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('main'))
