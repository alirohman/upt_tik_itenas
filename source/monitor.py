#!/usr/bin/python

import os
import sys
import bsddb
import json
import psutil
import subprocess
import sched
import commands
import re
import time

from multiprocessing import Process
from datetime import datetime, timedelta

def monitor(ip, initial_bw_down, initial_bw_up):
    CUR_DIR = os.path.abspath(os.path.dirname(__file__))
    os.system('rm -rf '+os.path.join(CUR_DIR,'__db*'))
    os.system('rm -rf '+os.path.join(CUR_DIR,'performance.db'))
    perform = bsddb.btopen(os.path.join(CUR_DIR,'performance.db'), 'c')
    memory_percent = psutil.phymem_usage().percent
    memory_total = int(psutil.phymem_usage().total)
    memory_usage = int(psutil.phymem_usage().used)
    memory_free = int(psutil.phymem_usage().free)
    vmemory_percent = psutil.virtmem_usage().percent
    vmemory_total = int(psutil.virtmem_usage().total)
    vmemory_usage = int(psutil.virtmem_usage().used)
    vmemory_free = int(psutil.virtmem_usage().free)

    hdd_usage_percent = psutil.disk_usage('/').percent
    hdd_usage_total = int(psutil.disk_usage('/').total)
    hdd_usage_used = int(psutil.disk_usage('/').used)
    hdd_usage_free = int(psutil.disk_usage('/').free)
        
    cpu_usage_percent = psutil.cpu_percent()
    cpu_num = psutil.NUM_CPUS

    bw_down = psutil.network_io_counters().bytes_recv
    bw_up = psutil.network_io_counters().bytes_sent
    bw_down = initial_bw_down + bw_down
    bw_up = initial_bw_up + bw_up
    initial_bw_down = bw_down
    initial_bw_up = bw_up

    default = {}
    for i in psutil.get_pid_list():
        try:
            x = psutil.Process(i).get_connections()
        except:
            pass
        else:
            if x:
                for y in x:
                    if y.status == 'LISTEN':
                        port = y.local_address[-1]
                        if port not in default.keys():
                            name = psutil.Process(i).name
                            mem = psutil.Process(i).get_memory_percent()
                            proc = psutil.Process(i).get_cpu_percent()
                            default.update({port:[name, mem, proc]})
    
    label = open(os.path.join(CUR_DIR,'label.txt'), 'r').read()
    data = {'memory':memory_percent, 
            'ip':ip, 
            'virtual_memory': vmemory_percent,
            'mem_total': memory_total,
            'mem_usage': memory_usage,
            'mem_free': memory_free,
            'vmem_total': vmemory_total,
            'vmem_usage': vmemory_usage,
            'vmem_free': vmemory_free,
            'cpu': cpu_usage_percent,
            'hdd': hdd_usage_percent,
            'hdd_total': hdd_usage_total,
            'hdd_free': hdd_usage_free,
            'hdd_used': hdd_usage_used,
            'label': label[:-1],
            'bw_down': bw_down,
            'bw_up': bw_up,
            'cpu_num': cpu_num,
            'listen': default,
            }
    str_data = json.dumps(data)
    perform['%s-%s-%s-%s-%s-%s-%s' % (datetime.now().year,
                                      datetime.now().month,
                                      datetime.now().day, 
                                      datetime.now().hour, 
                                      datetime.now().minute,
                                      datetime.now().second,
                                      datetime.now().microsecond
                                      )] = str_data

def go(ip):
    initial_bw_down = 0
    initial_bw_up = 0
    while True:
        monitor(ip, initial_bw_down, initial_bw_up)
        time.sleep(1)
if __name__ == "__main__":
    CUR_DIR = os.path.abspath(os.path.dirname(__file__))
    try:
        ch = os.fork()
        if ch > 0:
            open('/tmp/monitor.pid', 'w').write(str(ch))
            sys.exit(0)
    except OSError, e:
        sys.exit(1)
    try:
        rest_run = subprocess.Popen(['python', os.path.join(CUR_DIR, 'rest_service.py'), 'start'])
        pid_rest = rest_run.pid
        ip = commands.getoutput('ifconfig eth0 | grep "inet addr"')
        if re.search(r'inet addr:([0-9\.]+)', ip):
            ip = re.search(r'inet addr:([0-9\.]+)', ip).groups()[0]
            go(ip)
        else:
            ip = 'Unknown'
            go(ip)
    except Exception, err:
        print err

