#!/usr/bin/python

from stompy import Stomp
from datetime import datetime, timedelta
from subprocess import Popen

import os
import socket
import multiprocessing
import signal
import sys
import re
import commands
import json
import sched
import time
import urllib2
import pymongo
import smtplib
import gammu

# database define (database name = upt, database collection = deploy, log & checker)
db = pymongo.Connection()
coll_deploy = db.upt.deploy
coll_log = db.upt.log
coll_checker = db.upt.checker

# data gmail sender untuk email alert
ADMIN_GMAIL_USER = 'uptitenas@gmail.com'
ADMIN_GMAIL_PASS = '1515sukses'

port_list = [80, 443, 23, 25, 3306, 21]
MAX_CPU = 75
MAX_MEMORY = 75
DELAY_TIME = 65

# send mail function dengan 2 argumen (message dan email tujuan), menggunakan smtplib
def send_mail(message, to):
    try:
        email_acc = ADMIN_GMAIL_USER
        email_pwd = ADMIN_GMAIL_PASS
        smtpserver = smtplib.SMTP("smtp.gmail.com",587)
        print 'connecting to smtp.gmail.com:587'
        smtpserver.ehlo()
        smtpserver.starttls()
        smtpserver.ehlo
        print 'ehlo smtp.gmail.com:587'
        smtpserver.login(email_acc, email_pwd)
        print 'logged in to smtp.gmail.com:587'
        header = 'To:' + to + '\n' + 'From: upt itenas monitoring <'+email_acc+'>\n' + 'Subject: ALERT!!\n'
        msg = header + '\n'+message+'\n\n'
        smtpserver.sendmail(email_acc, to, msg)
        print 'sending mail to: %s' % to
        smtpserver.close()
    except Exception, err:
        print 'failed send email: %s' % err
        return False
    else:
        print 'sending mail successful'
        return True

# send sms function dengan 2 argumen (message dan nomor tujuan), menggunakan subprocess execution ke script sms_sender.py
def send_sms(message, to):
#    os.system('python sms_sender.py "%s" %s' %(message,to))
    Popen(['python','sms_sender.py','"%s"' % message, to], close_fds=True)
    return True

# callback function untuk timeout stomp sender
def signal_timeout(signum, frame):
    raise Exception

# fungsi yg di call periodik utk checking data ke restful web service target
def schedule_monitor():
    stomp = Stomp(hostname='localhost', port=54123)
    stomp.connect()
    stomp.subscribe({'destination':'/monitor',
                     'ack':'client'})

    signal.signal(signal.SIGALRM, signal_timeout)
    try:
        up_down_data = []
        # lookup ke database deploy collection dan tiap data di collection akan di cek restful web servicenya
        for i in coll_deploy.find():
            # set timestamp untuk kesamaan data keseluruhan
            timestamp = '%s-%s-%s-%s-%s-%s-%s' % (datetime.now().year,
                                                  datetime.now().month,
                                                  datetime.now().day, 
                                                  datetime.now().hour, 
                                                  datetime.now().minute,
                                                  datetime.now().second,
                                                  datetime.now().microsecond
                                                  )

            try:
                # hit url get data untuk mendapatkan monitoring data berbentuk json response dari restful
                data = urllib2.urlopen('http://%s:1212/get_data' % i['ip']).read()
                # load data dari json string ke json object (dictionary di dlm list)
                data = json.loads(data)
                # loop data yg diambil dan tiap element di store ke db & diupdate ke template
                for x in data:
                    # set kebutuhan data monitoring sesuai collection pada 1 variabel dictionary
                    db_data = {'ip': i['ip'],
                               'key': i['key'],
                               'label':i['label'],
                               'timestamp': datetime(int(x.keys()[0].split('-')[0]), 
                                                     int(x.keys()[0].split('-')[1]), 
                                                     int(x.keys()[0].split('-')[2]), 
                                                     int(x.keys()[0].split('-')[3]), 
                                                     int(x.keys()[0].split('-')[4]), 
                                                     int(x.keys()[0].split('-')[5])),
                               'cpu': x.values()[0]['cpu'],
                               'memory': x.values()[0]['memory'],
                               'hdd':x.values()[0]['hdd'],
                               'virtual_memory': x.values()[0]['virtual_memory'],
                               'bw_up': x.values()[0]['bw_up'],
                               'bw_down': x.values()[0]['bw_down'],
                               'cpu_num': x.values()[0]['cpu_num'],
                               'mem_total': x.values()[0]['mem_total'],
                               'mem_free': x.values()[0]['mem_free'],
                               'mem_usage': x.values()[0]['mem_usage'],
                               'vmem_total': x.values()[0]['vmem_total'],
                               'vmem_free': x.values()[0]['vmem_free'],
                               'vmem_usage': x.values()[0]['vmem_usage'],
                               'listen': x.values()[0]['listen'],

                    }
                    # set port default untuk di cek dari sisi luar (3way handshake)
                    for y in port_list:
                        # TCP socket connection
                        try:
                            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                            s.connect((i['ip'], y))
                        except Exception:
                            db_data.update({'port_%s'%y:False})
                            coll_deploy.update({'ip': i['ip'], 'key': i['key']}, {'$set': {'port_%s'%y:False, 'timestamp': timestamp}}) # update data ke collection deploy
                        else:
                            db_data.update({'port_%s'%y:True})
                            coll_deploy.update({'ip': i['ip'], 'key': i['key']}, {'$set': {'port_%s'%y:True, 'timestamp': timestamp}}) # update data ke collection deploy

                    coll_log.insert(db_data) # insert semua data monitoring ke collection log
                    print 'Insertion data into db: %s' % db_data
                delete = urllib2.urlopen('http://%s:1212/remove_data' % i['ip']).read() # delete semua data di target ketika semua data berhasil di store ke db pusat
            except Exception, err:                
                up = False # jika gagal koneksi maka set variabel up/down target False
                db_data = {'ip': i['ip'],
                           'key': i['key'],
                           'label':i['label'],
                           'timestamp': datetime.now(),
                           'cpu': 0,
                           'memory': 0,
                           'virtual_memory': 0,
                           'hdd': 0,
                           'bw_up': 0,
                           'bw_down': 0,
                           'cpu_num': None,
                           'mem_total': None,
                           'mem_free': None,
                           'mem_usage': None,
                           'vmem_total': None,
                           'vmem_free': None,
                           'vmem_usage': None,
                           'listen': None,

                    }
                # checking poet open dari sisi luar (koneksi 3 way handshake ke host target)
                for y in port_list:
                    try:
                        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                        s.connect((i['ip'], y))
                    except Exception:
                        db_data.update({'port_%s'%y:False})
                        coll_deploy.update({'ip': i['ip'], 'key': i['key']}, {'$set': {'port_%s'%y:False, 'timestamp': timestamp}})
                    else:
                        db_data.update({'port_%s'%y:True})
                        coll_deploy.update({'ip': i['ip'], 'key': i['key']}, {'$set': {'port_%s'%y:True, 'timestamp': timestamp}})
                coll_log.insert(db_data)
            else:
                up = True # jika sukses koneksi maka set variabel up/down target False
                
                
            # statement diluar exception untuk update timestamp
            coll_deploy.update({'ip': i['ip'], 'key': i['key']}, {'$set': {'up': up, 'timestamp': timestamp}})
            
            # exception untuk variabel penampun data2 monitoring yg diambil dari restful web service
            try:
                dates = data[-1].keys()[0]
            except Exception:
                dates = timestamp
            try:
                memory = data[-1].values()[0]['memory']
            except Exception:
                memory = 0
            try:
                v_memory = data[-1].values()[0]['virtual_memory']
            except Exception:
                v_memory = 0
            try:
                hdd = data[-1].values()[0]['hdd']
            except Exception:
                hdd = 0
            try:
                cpu = data[-1].values()[0]['cpu']
            except Exception:
                cpu = 0
            try:
                bw_up = data[-1].values()[0]['bw_up']
            except Exception:
                bw_up = 0
            try:
                bw_down = data[-1].values()[0]['bw_down']
            except Exception:
                bw_down = 0

            # checking untuk alert notification
            if cpu > MAX_CPU and memory > MAX_MEMORY or not up:
                to = i['email']
                to_sms = i['hp']
                
                try:
                    if not list(coll_checker.find())[-1]['sms'] and not list(coll_checker.find())[-1]['email']:
                        if cpu > MAX_CPU and memory > MAX_MEMORY:
                            mail_sent = send_mail('Perhatian! Penggunaan memory dan CPU lebih dari 80%', to)
                            sms_sent = send_sms('Perhatian! Penggunaan memory dan CPU lebih dari 80%', to_sms)
                        if not up:
                            mail_sent = send_mail('Perhatian! Server %s DOWN! sejak tanggal: %s' %(i['ip'], datetime.now()), to)
                            sms_sent = send_sms('Perhatian! Server %s DOWN! sejak tanggal: %s' %(i['ip'], datetime.now()), to_sms)
                        coll_checker.insert({'datetime':datetime.now(), 'mail': mail_sent, 'sms': sms_sent})
                except Exception:
                    if cpu > MAX_CPU and memory > MAX_MEMORY:
                        mail_sent = send_mail('Perhatian! Penggunaan memory dan CPU lebih dari 80%', to)
                        sms_sent = send_sms('Perhatian! Penggunaan memory dan CPU lebih dari 80%', to_sms)
                    if not up:
                        mail_sent = send_mail('Perhatian! Server %s DOWN! sejak tanggal: %s' %(i['ip'], datetime.now()), to)
                        sms_sent = send_sms('Perhatian! Server %s DOWN! sejak tanggal: %s' %(i['ip'], datetime.now()), to_sms)
                    coll_checker.insert({'datetime':datetime.now(), 'mail': mail_sent, 'sms': sms_sent})
        
            else:
                coll_checker.remove()
#            if memory > 0 or v_memory > 0:
                
            status_data = {'ip': i['ip'], 
                               'up': up,
                               'label': i['label'],
                               'key': i['key'],
                               'memory': memory,
                               'virtual_memory': v_memory,
                               'hdd': hdd,
                               'cpu': cpu,
                               'bw_up': bw_up,
                               'bw_down': bw_down,
                               'timestamp': dates}

# port checking
            
            for x in port_list:
                try:
                    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    s.connect((i['ip'], x))
                except Exception:
                    status_data.update({'port_%s'%x:False})
                    coll_deploy.update({'ip': i['ip'], 'key': i['key']}, {'$set': {'port_%s'%x:False, 'timestamp': timestamp}})
                else:
                    status_data.update({'port_%s'%x:True})
                    coll_deploy.update({'ip': i['ip'], 'key': i['key']}, {'$set': {'port_%s'%x:True, 'timestamp': timestamp}})
            
            # tambah list up_down data yg kosong pada awalnya.. ini untuk keperluan send data ke template
            up_down_data.append(status_data)
        
        # starting up signal alarm timeout (1sec) sender
        signal.alarm(1)
        print 'sending data to template: %s' % up_down_data
        # send data menggunakan stomp protokol ke template berupa json data
        stomp.send({'destination': '/monitor',
                    'body': json.dumps(up_down_data), # dumping list object ke json string
                    'persistent': 'true'})
        # end signal alarm
        signal.alarm(0)
    except Exception, err:
        print err
        stomp.disconnect() # disconnect stomp jika semua proses terdapat kegagalan

# main function yg dipanggil loop tak hingga dengan delay 65 sec untuk menjalankan function schedule_monitor
def go():
    while True:
        try:
            schedule_monitor()
        except Exception, err:
            print err
        time.sleep(DELAY_TIME)

# module atau script style detection
if __name__ == '__main__':    
    go() # running main function go()
